# These steps will be performed by the Gitpod prebuild

./autogen.sh
. ./gitpod-prebuild-configuration.sh
make clean
${BEAR} make -j$(nproc)
make -j$(nproc) check

if [[ "${WITH_COVERAGE}" == "YES" ]]; then
  cleancov
  make -j$(nproc) test_bitcoin.coverage/.dirstamp
fi

