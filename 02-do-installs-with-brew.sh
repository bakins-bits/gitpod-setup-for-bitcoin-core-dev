echo '🟢 do installs with brew - dependencies zstd gcc@11'

brew install zstd gcc@11
if brew doctor check_for_unlinked_but_not_keg_only;  then
  echo '🟢 dependencies installed ok'
else
  echo '⛔ dependencies did not install ok, reinstalling'
  brew reinstall zstd gcc@11
fi

echo '🟢 do installs with brew - cmake ccache bear'
brew install cmake ccache bear

echo '🟢 done installing with brew'
