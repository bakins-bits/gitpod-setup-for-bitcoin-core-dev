# These steps will be performed by the Gitpod prebuild

pushd ${BTCDEB_ROOT}
git checkout force-logging-option
./autogen.sh
./configure --enable-dangerous
make -j$(nproc)
sudo make install-binPROGRAMS prefix=/workspace
popd
