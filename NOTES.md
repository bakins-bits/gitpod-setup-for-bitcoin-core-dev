- Current versions on Gitpod's `-full` image (2022-04-09):
  - clang/clang++ - 15.0.0+...
  - lld - 15.0.0
  - lldb - not installed
  - gcc/g++ - 9.2.0
  - gdb - 9.2
  - cmake - 3.22.3
  - ccache - not installed

- lldb, when installed (with python3-lldb) _doesn't work_.
  - That is, it issues an error about a "missing interpreter" and then
    goes into its REPL.  Actually, it may indeed work regardless of that
    error, I haven't tried it yet.