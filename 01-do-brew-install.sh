echo '🟢 installing brew:'

cd /home/gitpod

sudo mkdir -p /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv

sudo chown gitpod:gitpod /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv

wget -O brew-install.sh https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh
chmod +x brew-install.sh
NONINTERACTIVE=1 ./brew-install.sh

echo '🟢 set up environment in current shell and permanent for bash'

test -d ~/.linuxbrew && echo 'Set env from ~/.linuxbrew' && eval "$(~/.linuxbrew/bin/brew shellenv)"
test -d /home/linuxbrew/.linuxbrew && echo 'Set env from /home/linuxbrew/.linuxbrew' && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> ~/.bashrc.d/Z-brew
chmod +x ~/.bashrc.d/Z-brew

echo '🟢 done installing brew'
