FROM gitpod/workspace-python:latest

SHELL ["/bin/bash", "-c"]

USER root

# C++ + Python specialized for Bitcoin Core development:
# From https://github.com/bitcoin/bitcoin/blob/master/doc/build-unix.md and
# also https://github.com/bitcoin/bitcoin/blob/master/doc/dependencies.md

# Starting from PYTHON add LANG-C and BREW; result should be a much lighter C++ container than Gitpod's `workspace-full`

RUN    apt update \
    && DEBIAN_FRONTEND=noninteractive apt install -yq --no-install-recommends \
automake \
autotools-dev \
bsdmainutils \
build-essential \
keyboard-configuration \
    && wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && sudo ./llvm.sh all \
    && DEBIAN_FRONT_END=noninteractive apt install -yq --no-install-recommends \
gdb \
lcov \
libboost-all-dev \
libboost-dev \
libdw-dev \
libevent-dev \
libminiupnpc-dev \
libnatpmp-dev \
libqrencode-dev \
libqt5core5a \
libqt5dbus5 \
libqt5gui5 \
libsqlite3-dev \
libtool \
libunwind-dev \
libzmq3-dev \
libzstd-dev \
locales \
pkg-config \
python3-zmq \
qttools5-dev \
qttools5-dev-tools \
qtwayland5 \
systemtap \
systemtap-sdt-dev

USER gitpod

RUN    echo "/usr/bin symlinks after clang+llvm install:" && ls -al /usr/bin | grep -E '^l'           \
    && echo '🟢 installing brew:'                                                                     \
    && sudo mkdir -p /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv            \
    && echo "before chown:" && echo "etc:" && ls -al /etc && echo "workspace:" && ls -al /workspace   \
    && sudo chown gitpod:gitpod /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv \
    && echo "after chown:" && echo "etc:" && ls -al /etc && echo "workspace:" && ls -al /workspace    \
    && wget -O brew-install.sh https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh     \
    && chmod +x brew-install.sh                                                                       \
    && NONINTERACTIVE=1 ./brew-install.sh                                                             \
    && echo '🟢 done installing brew'

# n.b.: Can't actually put anything persistent under `/workspace` in here because in Gitpod `/workspace`
# is an _external mountpoint_ and thus will hide anything there in this container.  But making sure env
# vars exist is fine, even putting the directories in the path (where nonexistent directories are ignored)
# is ok.  Also, set `PKG_CONFIG_PATH` because `brew` _stupidly_ overwrites it
ENV CCACHE_DIR="/workspace/.ccache"                              \
    PYENV_ROOT="/workspace/.pyenv"                               \
    HOMEBREW_NO_AUTO_UPDATE=1

COPY --chown=gitpod:gitpod *.md *.sh .gitpod.yml /etc/gitpod-setup/
COPY --chown=gitpod:gitpod .vscode/*             /etc/gitpod-setup/.vscode/

RUN    sudo chown gitpod:gitpod llvm.sh brew-install.sh                         \
    && cp llvm.sh brew-install.sh /etc/gitpod-setup                             \
    && sudo apt remove -y cmake                                                 \
    && echo default PKG_CONFIG_PATH=$(pkg-config --variable pc_path pkg-config)

RUN    echo '🟢 install dependences` with brew'                \
    && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"  \
    && brew install glibc || true                              \
    && brew list                                               \
    && brew doctor || true                                     \
    && brew install zstd gcc@11                                \
    && while ! brew doctor check_for_unlinked_but_not_keg_only check_for_broken_symlinks; do echo '⛔ dependencies bad' && brew reinstall zstd gcc@11 || true; done \
    && echo '🟢 install main apps with brew'                   \
    && brew install cmake ccache bear                          \
    && echo '🟢 done installing with brew'

RUN    echo post-brew PKG_CONFIG_PATH=${PKG_CONFIG_PATH}                          \
    && sudo apt-get clean -y                                                      \
    && sudo rm -rf /var/cache/debconf/* /var/lib/apt/lists/* /tmp/* /var/tmp/*    \
    && echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/gitpod/.bashrc.d/Z-brew \
    && cat  /home/gitpod/.bashrc.d/Z-brew                                         \
    && echo 'From gitpod-setup-for-bitcoin-core-dev ' `date +"%Y-%m-%d %T %Z"` > /etc/gitpod-setup/00-this-container-is.txt

# Leaving out precise BDB version - must build with --with-incompatible-bdb or --without-bdb
# Leaving out lldb-15 & python3-lldb-15 - suddenly including lldb-15 causes clang/clang++ to segfault
# **TODO:** Try this again now that -15 is released

# The explicit install of keyboard-configuration is because it is required by something or
# other in the QT set but it demands (stupidly!) to ask the user _which_ keyboard layout to
# install, which breaks the docker build.  So you have to explicitly install it in a mode
# where you tell it to shut up. (via 'noninteractive')

# Consider adding gitpod's vnc to get X server + VNC GUI remoting for GUI work

# **UPDATES:**
# 2022-09-09: No longer have to build `ccache` & `bear` from sources but still need to explicitly
#             install _some_ dependencies (and those may error (!!!) the first time); _do_ need to
#             set PATH and brew's env vars using `brew shellenv`  But much faster now.
# 2022-08-04: LLVM-15 package no longer exists since it is getting ready for release, switching to "automatic install"
#             script which installs latest _released_ (as opposed to under development)
# BUT WAIT:   _Today_ script _is_ installing -15 _from packages_.
# 2022-08-04: Now building `ccache` & `bear` requires both `--build-from-source` flag _and_ building `bear`
#             dependencies separately.
