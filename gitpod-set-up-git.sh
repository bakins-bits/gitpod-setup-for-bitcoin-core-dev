pushd /workspace/bitcoin

echo '🟢 add bitcoin/bitcoin as upstream remote'
git config --local remote.upstream.url https://github.com/bitcoin/bitcoin.git
git config --local remote.upstream.fetch +refs/heads/*:refs/remotes/upstream/*
git config --add --local remote.upstream.fetch +refs/pull/*/head:refs/remotes/upstream/pr/*
echo '🟢 done adding bitcoin/bitcoin as upstream remote'

popd
