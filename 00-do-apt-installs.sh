echo '🟢 apt update:'
sudo apt update

echo '🟢 initial apt installs:'
sudo DEBIAN_FRONTEND=noninteractive apt install -yq --no-install-recommends \
automake \
autotools-dev \
bsdmainutils \
build-essential \
keyboard-configuration

echo '🟢 llvm install:'
wget https://apt.llvm.org/llvm.sh
chmod +x llvm.sh
sudo ./llvm.sh all

echo '🟢 subsequent apt installs:'
sudo DEBIAN_FRONTEND=noninteractive apt install -yq --no-install-recommends \
gdb \
lcov \
libboost-all-dev \
libboost-dev \
libdw-dev \
libevent-dev \
libminiupnpc-dev \
libnatpmp-dev \
libqrencode-dev \
libqt5core5a \
libqt5dbus5 \
libqt5gui5 \
libsqlite3-dev \
libtool \
libunwind-dev \
libzmq3-dev \
libzstd-dev \
pkg-config \
python3-zmq \
qttools5-dev \
qttools5-dev-tools \
qtwayland5 \
systemtap \
systemtap-sdt-dev

echo "/usr/bin symlinks after clang+llvm install:" && ls -al /usr/bin | grep -E '^l'
