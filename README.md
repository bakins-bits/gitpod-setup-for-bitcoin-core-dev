## Do Bitcoin Core development in a Gitpod workspace

This repo enables Bitcoin Core development via Gitpod.  Consists of a Docker image
that has all of the necessary Bitcoin Core dependencies, plus Gitpod scripts (e.g.,
`.gitpod.yml`) to set things up properly (e.g., with `ccache`, ready for code
coverage runs, etc.)

At this time you can build Bitcoin Core using gcc or clang, release or debug,
with GUI or without.  You can write/modify/debug unit and functional tests,
and you can run everything getting code coverage reports.

### Usage

1. Fork [Bitcoin Core](https://github.com/bitcoin/bitcoin) and
   [this repository](https://gitlab.com/bakins-bits/gitpod-setup-for-bitcoin-core-dev).
1. Edit `.gitpod.yml` in this repository: Change the line `url:` immediately under
   `additionalRepositories:` to point to your Bitcoin Core fork repository.  Commit
   that and push.
1. Have VSCode installed on your desktop, with C++ extensions installed and enabled
   (plus whatever other extensions, settings, customizations) you're happy with.
1. Install the [Gitpod VSCode extension](https://marketplace.visualstudio.com/items?itemName=gitpod.gitpod-desktop).
1. Launch Gitpod.io _on your fork of this repository_.  To do that, use a URL like:
   ```
   https://gitpod.io/#https://gitlab.com/bakins-bits/gitpod-setup-for-bitcoin-core-dev
   ```
   Which is simply adding the prefix `https://gitpod.io/#` to the url of your fork.
1. There will be a delay here the first time while it is preparing everything:
   1. Pulling the base container
   1. Making a workspace container
   1. Configuring and building Bitcoin Core (on branch `master`)
   1. Running the unit tests.

   - When you start using Gitpod to manage your workspaces, as described in their
     documentation, this will get a _lot_ faster as you'll be able to take advantage
     of _prebuilt_ workspaces where all of the above is already done when you click
     the link to open a workspace.  (One of Gitpod's major value propositions!)

2. When it asks tell it to open in your _desktop VSCode_.
3. It'll open up, you'll have to inform it that you're connecting to Linux and the
   approve the remote and approve that you trust all the code.
4. You'll see in the terminal window that tests have run (and passed).
5. Party on!
   - Develop normally!
   - You can leave workspaces and come back to them later.  When you do that _only
     content in `/workspace` is saved and restored!_  (Nothing else,
     including `/home/gitpod`.) So keep that in mind.
   - Read [`gitpod.io` documentation](https://www.gitpod.io/docs/) for more information
     on how to use Gitpod workspaces: creating projects, using multiple workspaces at
     the same time, managing workspaces including stop/resume, etc.  Also what resources
     the free Gitpod account gives you, and what you get in addition when you subscribe
     to higher tiers.

### Where everything is

- `/workspace/bitcoin` is the Bitcoin Core repository, ready to go.
- `/workspace/gitpod-setup-for-bitcoin-core-dev` is your fork of this setup repository.
- `/workspace/bitcoin/gitpod-prebuild-configuration.sh` symlinks off
  to `/workspace/gitpod-setup-for-bitcoin-core-dev` - you can edit this file to
  change your configuration (e.g., `gcc` vs `clang`, code coverage or not).
  - "source" this file in order to do the configuration (remember to `make clean` after
    this!)
    ```
    # "sourcing" the configuration:
    . ./gitpod-prebuild-configuration.sh
    ```
- `/workspace/bitcoin/gitpod-prebuild.sh` (which also symlinks off to your copy of
  this repo) is what you "source" to _both_ configure _and_ build&run tests.  (It'll
  do `make clean` first.)

### Limitations

- Can't do your work in the browser, the way Gitpod promotes in their tutorials.
  Must use a desktop VSCode (which works totally fine). This is because the
  VSCode C++ extension, provided by Microsoft, is not open source, and has license
  restrictions that mean it can't run in anything except the genuine officially-built
  VSCode - in particular, the Gitpod browser VSCode is not allowed. (And the
  extension might even be hard-coded to _not_ run anywhere except the genuine VSCode...)

- Launches (and builds) only the `master` branch of Bitcoin Core right now.  Works fine
  of course for whatever branch you're working in, but you'll have to change branches
  and build there manually, not take full advantage of Gitpod prebuilds.

- Can't build with BDB for old-style wallets.  I didn't bother to fetch and build
  the correct BDB version, and for some reason I didn't bother to investigate, clang
  builds don't work with `--with-incompatible-bdb`.  So just don't do it.  Build only
  the new-fangled SQLite3 wallets.

- Can't actually _run_ Bitcoin Core's daemon properly because there's no enough room
  for a blockchain.  (Even testnet's blockchain, at ~25GB, when added to the space
  needed for the development environment and a Bitcoin Core build, is too large for
  a Gitpod workspace's limits.)  And Bitcoin Core requires that the blockchain itself
  and all of the associated databases (indexes, etc.) be on a local disk.

  - **N.B.:** The functional tests run fine, so regtest mode works.

- Currently haven't tried to _run_ the GUI, since that'll need to have the X protocol
  tunnelled out to your desktop.  Gitpod has ways to do it, no problem, I just haven't
  done it myself.

- Currently, haven't tried gperf or any of the sanitizer builds.

- Doesn't have `lldb`, only `gdb`
  - (`lldb` installed by `apt install` is broken for some reason, don't know why)

### What are the moving parts?

There are three containers involved:
1. Gitpod provides a base container, `gitpod/workspace-base`, that is set up to work
   as a Gitpod instance and provides basic support, including docker support.  There's
   also a container built on that which provides a system `python3` and `pyenv` -
   `gitpod/workspace-python`.
1. With that as a base image, I build here, with `Dockerfile` (and `.gitlab-ci.yml`),
   a container that has everything you need for C++ development, including `ccache`,
   plus all of the Bitcoin Core dependencies.  It sets up `ccache` and `pyenv` to
   work out of `/workspace` instead of `/home/gitpod` so that the settings and the
   compile cache and the built Python versions  can be preserved across workspace
   stop/resume.

   That container is pushed to the Gitlab registry and is available publicly.
1. Then, Gitpod starts to work.  For each workspace a _custom workspace_ container
   is built, by the file `.gitpod.Dockerfile`.  Nothing much is added here.
1. Finally, Gitpod uses `.gitpod.yml` to prepare its own container, which is how
   you get _prebuilt_ workspaces so (once you get going, not the first time) a
   bunch of stuff is done for you by the time you open the workspace:
   - Bitcoin Core is configured (according to `gitpod-configuration.sh` which you
     can change to get whatever configuration you want),
   - Bitcoin Core is fully built according to that configuration (in branch
     `master` only, with with limitations as described above),
   - And all unit tests run with their summary results displayed (in the terminal
     window).)

### ROADMAP

1. Confirm associated dev tools work (e.g., clang-tidy) or make them work.

2. Improve configurability of the Gitpod init script so that user can easily prebuild
   workspaces with the desired configuration (build/release, gcc/clang, branch,
   etc.).  Possibly via env vars, for which Gitpod has support via both their
   dashboard UI and via simply putting them in URLs when opening a workspace.

3. Get a workflow for being able to export built Bitcoin Core daemons (and tools)
   to a Docker image, then being able to run that image somewhere where sufficient
   local storage is available for a blockchain.  (Perhaps in the cloud, perhaps
   on a local desktop/server.)

4. Get rid of `.gitpod.Dockerfile` by using the `Dockerfile` container only.
   - Should make workspace startup faster?  Or at least prebuilds?

5. Get `lldb` to work.

6. Clean up `.gitpod.yaml`, especially things like setting bash aliases.
