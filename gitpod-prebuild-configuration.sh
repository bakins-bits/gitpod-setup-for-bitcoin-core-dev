# Set here the Bitcoin Core configuration to build built by Gitpod during the
# prebuild of the workspace (so its ready to go when you start the workspace).
# Not only will all the binaries be there to use immediately, but ccache will
# be nicely warm for this configuration.

OPTIONS_FOR_GCC=""
OPTIONS_FOR_CLANG="CXX=clang++ CC=clang"

OPTIONS_FOR_DEBUG="--enable-debug --disable-hardening --disable-bench --disable-fuzz --disable-fuzz-binary --enable-usdt --enable-threadlocal"
OPTIONS_FOR_RELEASE=""
OPTIONS_FOR_RELEASE_WITH_SYMBOLS="--enable-debug"  # don't know if this works: might disable optimizations too

OPTIONS_FOR_GUI="-with-gui-qt5 --enable-gui-tests --with-qtdbus --with-qrencode"
OPTIONS_FOR_NO_GUI="--without-gui --disable-gui-tests"

OPTIONS_FOR_CXX_BACKTRACE='CXXFLAGS=-ggdb3 --unwindlib=libgcc -ldw -fno-pie -no-pie'
OPTIONS_FOR_LD_BACKTRACE='LDFLAGS=-rdynamic -L/usr/lib/x86_64-linux-gnu'
OPTIONS_FOR_COVERAGE="--enable-lcov --enable-lcov-branch-coverage"

# Set these appropriately for the configuration you want:

USE_COMPILER=${OPTIONS_FOR_CLANG}
USE_BUILD_TYPE=${OPTIONS_FOR_DEBUG}
USE_GUI_OR_NOT=${OPTIONS_FOR_NO_GUI}
USE_ADDITIONAL_OPTIONS=${OPTIONS_FOR_COVERAGE}
#   e.g., --enable-lto --with-sanitizers=...


( set -x ; \
  ./configure ${USE_COMPILER} ${USE_BUILD_TYPE} ${USE_GUI_OR_NOT} \
              --with-boost=yes --with-utils --with-libs --with-daemon \
              --with-sqlite=yes --without-bdb --disable-man \
              ${USE_ADDITIONAL_OPTIONS} \
              'CXXFLAGS=-ggdb3 --unwindlib=libgcc -ldw -fno-pie -no-pie' \
              'LDFLAGS=-rdynamic -L/usr/lib/x86_64-linux-gnu' \
              --disable-silent-rules
)

if [ "${USE_COMPILER}" = "${OPTIONS_FOR_CLANG}" ]; then
  COMPILER_IS="CLANG"
  BEAR="bear --"
else
  COMPILER_IS="GCC"
  BEAR=""
fi
if [[ "${OPTIONS_FOR_COVERAGE}" == *"${USE_ADDITIONAL_OPTIONS}"* ]]; then
  WITH_COVERAGE="YES"
else
  WITH_COVERAGE=""
fi

export COMPILER_IS
export BEAR
export WITH_COVERAGE

# see what's available with ./configure --help

# TODO: settable option for multiprocessing?
# TODO: settable option for fuzzing?
# TODO: settable option for perf?

# TODO: message chosen option groups (e.g., "OPTIONS_FOR_GUI") to console
