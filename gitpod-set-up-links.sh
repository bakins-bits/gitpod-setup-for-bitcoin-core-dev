pushd /workspace/bitcoin

echo '🟢 link prebuild scripts and other setup scripts and files'
ln -s ${SETUP_ROOT}/gitpod-prebuild-bitcoin.sh       ./gitpod-prebuild-bitcoin.sh
ln -s ${SETUP_ROOT}/gitpod-prebuild-btcdeb.sh        ./gitpod-prebuild-btcdeb.sh
ln -s ${SETUP_ROOT}/gitpod-prebuild-configuration.sh ./gitpod-prebuild-configuration.sh
ln -s ${SETUP_ROOT}/gitpod-set-up-links.sh           ./gitpod-set-up-links.sh
ln -s ${SETUP_ROOT}/gitpod-set-up-llvm-links.sh      ./gitpod-set-up-llvm-links.sh
ln -s ${SETUP_ROOT}/bitcoin-aliases                  ./bitcoin-aliases
rm .git/info/exclude
ln -s ${SETUP_ROOT}/.gitignore.global .git/info/exclude
ln -s ${SETUP_ROOT}/.lcovrc ~/.lcovrc
mkdir /workspace/data
ln -s ${SETUP_ROOT}/bitcoin.conf                      /workspace/data/bitcoin.conf
echo '🟢 done linking prebuild scripts and other setup scripts and files'

popd
